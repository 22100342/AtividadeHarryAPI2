//
//  ViewController.swift
//  ProvaFinalSegundaEtapaEnzoRusso
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Personagem: Decodable {
    let name: String
    let actor: String
    let image: String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var listaPersonagem:[Personagem] = []
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaPersonagem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let personagem = self.listaPersonagem[indexPath.row]
        print(personagem)
        
        cell.namePersonagem.text = personagem.name
        cell.nameActor.text = personagem.actor
        cell.imagemPersonagem.kf.setImage(with: URL(string: personagem.image))
        
        return cell
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    func getNewPersonagem() {
                AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of: [Personagem].self) { response in
                    if let personagem = response.value {
                        self.listaPersonagem = personagem
                    }
                    self.tableView.reloadData()
                }
        }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        getNewPersonagem()
        
    }

}

